# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Nobuhiro Iwamatsu <iwamatsu@nigauri.org>, 2016-2017,2019,2021
# UTUMI Hirosi <utuhiro78@yahoo.co.jp>, 2021
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: xfce-i18n@xfce.org\n"
"POT-Creation-Date: 2021-12-10 12:50+0100\n"
"PO-Revision-Date: 2021-12-10 11:50+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Japanese (http://www.transifex.com/xfce/xfce-apps/language/ja/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ja\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../src/main.c:46
msgid "Version information"
msgstr "バージョン情報を表示する"

#: ../src/main.c:50
msgid "Start in fullscreen mode"
msgstr "フルスクリーンモードで開始する"

#: ../src/main.c:54
msgid "Start a slideshow"
msgstr "スライドショーを開始する"

#: ../src/main.c:62
msgid "Show settings dialog"
msgstr "設定ダイアログを表示する"

#: ../src/main.c:96
#, c-format
msgid ""
"%s: %s\n"
"\n"
"Try %s --help to see a full list of\n"
"available command line options.\n"
msgstr "%s: %s\n\n%s --help で使用可能なすべてのコマンドライン\nオプションが表示されます。\n"

#: ../src/main_window.c:38 ../org.xfce.ristretto.desktop.in.h:3
msgid "Image Viewer"
msgstr "画像ビューアー"

#: ../src/main_window.c:41
msgid "Could not save file"
msgstr "ファイルを保存できませんでした"

#: ../src/main_window.c:42
msgid "Some files could not be opened: see the text logs for details"
msgstr "一部のファイルを開けませんでした: 詳細についてはテキストログを参照してください"

#: ../src/main_window.c:43
#, c-format
msgid "An error occurred when deleting image '%s' from disk"
msgstr "ディスクからイメージ '%s' を削除するときにエラーが発生しました"

#: ../src/main_window.c:44
#, c-format
msgid "An error occurred when sending image '%s' to trash"
msgstr "画像 '%s' をゴミ箱に送るときにエラーが発生しました"

#: ../src/main_window.c:329
msgid "_File"
msgstr "ファイル(_F)"

#. Icon-name
#: ../src/main_window.c:335
msgid "_Open..."
msgstr "開く(_O)..."

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:337
msgid "Open an image"
msgstr "画像を開きます"

#. Icon-name
#: ../src/main_window.c:341
msgid "_Save copy..."
msgstr "コピーを保存(_S)..."

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:343
msgid "Save a copy of the image"
msgstr "画像のコピーを保存します"

#. Icon-name
#: ../src/main_window.c:347
msgid "_Properties..."
msgstr "プロパティ(_P)..."

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:349
msgid "Show file properties"
msgstr "ファイルのプロパティを表示します"

#. Icon-name
#: ../src/main_window.c:353 ../src/main_window.c:372
msgid "_Edit"
msgstr "編集(_E)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:355
msgid "Edit this image"
msgstr "この画像を編集します"

#. Icon-name
#: ../src/main_window.c:359 ../src/preferences_dialog.c:218
#: ../src/properties_dialog.c:102
msgid "_Close"
msgstr "閉じる(_C)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:361
msgid "Close this image"
msgstr "画像を閉じます"

#. Icon-name
#: ../src/main_window.c:365
msgid "_Quit"
msgstr "終了(_Q)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:367
msgid "Quit Ristretto"
msgstr "Ristretto を終了します"

#: ../src/main_window.c:378
msgid "_Copy image to clipboard"
msgstr "画像をクリップボードにコピーする(_C)"

#: ../src/main_window.c:384
msgid "_Open with"
msgstr "アプリケーションで開く(_O)"

#: ../src/main_window.c:390
msgid "_Sort by"
msgstr "ソート順(_S)"

#. Icon-name
#: ../src/main_window.c:396
msgid "_Delete"
msgstr "削除(_D)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:398
msgid "Delete this image from disk"
msgstr "この画像をディスクから削除します"

#. Icon-name
#: ../src/main_window.c:402
msgid "_Clear private data..."
msgstr "プライベートデータのクリア(_C)..."

#. Icon-name
#: ../src/main_window.c:408
msgid "_Preferences..."
msgstr "設定(_P)..."

#: ../src/main_window.c:415
msgid "_View"
msgstr "表示(_V)"

#. Icon-name
#: ../src/main_window.c:421
msgid "_Fullscreen"
msgstr "全画面化(_F)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:423
msgid "Switch to fullscreen"
msgstr "フルスクリーンモードに切り替えます"

#. Icon-name
#: ../src/main_window.c:427
msgid "_Leave Fullscreen"
msgstr "元に戻す(_L)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:429
msgid "Leave Fullscreen"
msgstr "フルスクリーンモードを解除します"

#. Icon-name
#: ../src/main_window.c:433
msgid "Set as _Wallpaper..."
msgstr "壁紙に設定(_W)..."

#: ../src/main_window.c:440
msgid "_Zoom"
msgstr "拡大/縮小(_Z)"

#. Icon-name
#: ../src/main_window.c:446
msgid "Zoom _In"
msgstr "拡大(_I)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:448
msgid "Zoom in"
msgstr "画像を拡大表示します"

#. Icon-name
#: ../src/main_window.c:452
msgid "Zoom _Out"
msgstr "縮小(_O)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:454
msgid "Zoom out"
msgstr "画像を縮小表示します"

#. Icon-name
#: ../src/main_window.c:458 ../src/main_window.c:742
msgid "Zoom _Fit"
msgstr "ウィンドウに合わせる(_F)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:460
msgid "Zoom to fit window"
msgstr "表示サイズをウィンドウの大きさに合わせます"

#. Icon-name
#: ../src/main_window.c:464 ../src/main_window.c:748
msgid "_Normal Size"
msgstr "標準サイズ(_N)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:466
msgid "Zoom to 100%"
msgstr "ズーム率 100% で表示します"

#: ../src/main_window.c:471
msgid "_Default Zoom"
msgstr "デフォルトズーム (_D)"

#: ../src/main_window.c:478
msgid "_Rotation"
msgstr "回転(_R)"

#. Icon-name
#: ../src/main_window.c:484
msgid "Rotate _Right"
msgstr "右回転(_R)"

#. Icon-name
#: ../src/main_window.c:490
msgid "Rotate _Left"
msgstr "左回転(_L)"

#: ../src/main_window.c:497
msgid "_Flip"
msgstr "反転(_F)"

#: ../src/main_window.c:503
msgid "Flip _Horizontally"
msgstr "水平方向に反転(_H)"

#: ../src/main_window.c:509
msgid "Flip _Vertically"
msgstr "垂直方向に反転(_V)"

#: ../src/main_window.c:516
msgid "_Go"
msgstr "移動(_G)"

#. Icon-name
#: ../src/main_window.c:522
msgid "_Forward"
msgstr "進む(_F)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:524
msgid "Next image"
msgstr "次の画像を開きます"

#. Icon-name
#: ../src/main_window.c:528
msgid "_Back"
msgstr "戻る(_B)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:530
msgid "Previous image"
msgstr "前の画像に戻ります"

#. Icon-name
#: ../src/main_window.c:534
msgid "F_irst"
msgstr "先頭(_I)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:536
msgid "First image"
msgstr "最初の画像に切り替えます"

#. Icon-name
#: ../src/main_window.c:540
msgid "_Last"
msgstr "最後(_L)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:542
msgid "Last image"
msgstr "最後の画像を開きます"

#: ../src/main_window.c:547
msgid "_Help"
msgstr "ヘルプ(_H)"

#. Icon-name
#: ../src/main_window.c:553
msgid "_Contents"
msgstr "目次(_C)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:555
msgid "Display ristretto user manual"
msgstr "Ristretto ユーザーマニュアルを表示します"

#. Icon-name
#: ../src/main_window.c:559
msgid "_About"
msgstr "情報(_A)"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:561
msgid "Display information about ristretto"
msgstr "Ristretto についての情報を表示します"

#: ../src/main_window.c:566
msgid "_Position"
msgstr "位置(_P)"

#: ../src/main_window.c:572
msgid "_Size"
msgstr "大きさ(_S)"

#: ../src/main_window.c:578
msgid "Thumbnail Bar _Position"
msgstr "サムネイルバーの位置(_P)"

#: ../src/main_window.c:584
msgid "Thumb_nail Size"
msgstr "サムネイルの大きさ(_N)"

#. Icon-name
#: ../src/main_window.c:591
msgid "Leave _Fullscreen"
msgstr "元に戻す(_F)"

#. Icon-name
#: ../src/main_window.c:609
msgid "_Show Toolbar"
msgstr "ツールバー(_S)"

#. Icon-name
#: ../src/main_window.c:617
msgid "Show _Thumbnail Bar"
msgstr "サムネイルバー(_T)"

#. Icon-name
#: ../src/main_window.c:625
msgid "Show Status _Bar"
msgstr "ステータスバー(_B)"

#. Icon-name
#: ../src/main_window.c:637
msgid "file name"
msgstr "ファイル名"

#. Icon-name
#: ../src/main_window.c:643
msgid "file type"
msgstr "ファイルタイプ"

#. Icon-name
#: ../src/main_window.c:649
msgid "date"
msgstr "日付"

#. Icon-name
#: ../src/main_window.c:660
msgid "Left"
msgstr "左"

#: ../src/main_window.c:666
msgid "Right"
msgstr "右"

#: ../src/main_window.c:672
msgid "Top"
msgstr "上"

#: ../src/main_window.c:678
msgid "Bottom"
msgstr "下"

#: ../src/main_window.c:689
msgid "Very Small"
msgstr "とても小さい"

#: ../src/main_window.c:695
msgid "Smaller"
msgstr "小さい"

#: ../src/main_window.c:701
msgid "Small"
msgstr "少し小さい"

#: ../src/main_window.c:707
msgid "Normal"
msgstr "普通"

#: ../src/main_window.c:713
msgid "Large"
msgstr "少し大きい"

#: ../src/main_window.c:719
msgid "Larger"
msgstr "大きい"

#: ../src/main_window.c:725
msgid "Very Large"
msgstr "とても大きい"

#. Icon-name
#: ../src/main_window.c:736
msgid "_Smart Zoom"
msgstr "洗練されたズーム (_S)"

#: ../src/main_window.c:874
msgid "Images"
msgstr "画像"

#. Create Play/Pause Slideshow actions
#: ../src/main_window.c:945
msgid "_Play"
msgstr "再生(_P)"

#: ../src/main_window.c:945
msgid "Play slideshow"
msgstr "スライドショーを開始します"

#: ../src/main_window.c:946
msgid "_Pause"
msgstr "一時停止(_P)"

#: ../src/main_window.c:946
msgid "Pause slideshow"
msgstr "スライドショーを一時停止します"

#. Create Recently used items Action
#: ../src/main_window.c:949
msgid "_Recently used"
msgstr "最近開いたファイル(_R)"

#: ../src/main_window.c:949
msgid "Recently used"
msgstr "最近開いたファイル"

#: ../src/main_window.c:1074 ../src/main_window.c:1688
msgid "Press open to select an image"
msgstr "画像を選ぶには開くボタンを押してください。"

#: ../src/main_window.c:1630 ../src/main_window.c:1635
msgid "Open With Other _Application..."
msgstr "他のアプリケーションで開く(_A)..."

#: ../src/main_window.c:1645 ../src/main_window.c:1653
msgid "Empty"
msgstr "空"

#: ../src/main_window.c:1693
msgid "Loading..."
msgstr "読み込んでいます..."

#: ../src/main_window.c:2343
msgid "Choose 'set wallpaper' method"
msgstr "'壁紙に設定' 方式の選択"

#: ../src/main_window.c:2347 ../src/main_window.c:3481
#: ../src/main_window.c:3569 ../src/main_window.c:4234
#: ../src/privacy_dialog.c:149 ../src/xfce_wallpaper_manager.c:261
#: ../src/gnome_wallpaper_manager.c:190
msgid "_Cancel"
msgstr "キャンセル(_C)"

#: ../src/main_window.c:2350 ../src/main_window.c:4237
#: ../src/xfce_wallpaper_manager.c:267 ../src/gnome_wallpaper_manager.c:196
msgid "_OK"
msgstr "OK(_O)"

#: ../src/main_window.c:2360 ../src/preferences_dialog.c:436
msgid ""
"Configure which system is currently managing your desktop.\n"
"This setting determines the method <i>Ristretto</i> will use\n"
"to configure the desktop wallpaper."
msgstr "デスクトップを管理しているシステムを指定してください。\n<i>Ristretto</i> はこの方式でデスクトップの壁紙を設定します。"

#: ../src/main_window.c:2386 ../src/preferences_dialog.c:448
msgid "None"
msgstr "なし"

#: ../src/main_window.c:2390 ../src/preferences_dialog.c:452
msgid "Xfce"
msgstr "Xfce"

#: ../src/main_window.c:2394 ../src/preferences_dialog.c:456
msgid "GNOME"
msgstr "GNOME"

#: ../src/main_window.c:2909
msgid "Developers:"
msgstr "開発者: "

#: ../src/main_window.c:2921 ../org.xfce.ristretto.appdata.xml.in.h:3
msgid "Ristretto is an image viewer for the Xfce desktop environment."
msgstr "Ristretto は Xfce デスクトップ環境向けの画像ビューアーです。"

#: ../src/main_window.c:2929
msgid "translator-credits"
msgstr "Masato Hashimoto <hashimo@xfce.org>"

#: ../src/main_window.c:3479
msgid "Open image"
msgstr "画像を開く"

#: ../src/main_window.c:3481
msgid "_Open"
msgstr "開く(_O)"

#: ../src/main_window.c:3499
msgid ".jp(e)g"
msgstr ".jp(e)g"

#: ../src/main_window.c:3504
msgid "All Files"
msgstr "すべてのファイル"

#: ../src/main_window.c:3566
msgid "Save copy"
msgstr "コピーを保存"

#: ../src/main_window.c:3570
msgid "_Save"
msgstr "保存(_S)"

#: ../src/main_window.c:3754
#, c-format
msgid "Are you sure you want to send image '%s' to trash?"
msgstr "画像 '%s' をゴミ箱に捨ててもよろしいですか?"

#: ../src/main_window.c:3758
#, c-format
msgid "Are you sure you want to delete image '%s' from disk?"
msgstr "画像 '%s' をドライブから削除してもよろしいですか?"

#: ../src/main_window.c:3770
msgid "_Do not ask again for this session"
msgstr "次回からこのセッションについて確認を行わない(_D)"

#: ../src/main_window.c:4229
msgid "Edit with"
msgstr "アプリケーションで開く"

#: ../src/main_window.c:4250
#, c-format
msgid "Open %s and other files of type %s with:"
msgstr "%s とタイプが %s の他のファイルを以下のアプリケーションで開きます:"

#: ../src/main_window.c:4256
msgid "Use as _default for this kind of file"
msgstr "この種類のファイルのデフォルトアプリケーションにする(_D)"

#: ../src/main_window.c:4346
msgid "Recommended Applications"
msgstr "推奨アプリケーション"

#: ../src/main_window.c:4426
msgid "Other Applications"
msgstr "他のアプリケーション"

#: ../src/icon_bar.c:264
msgid "Orientation"
msgstr "方向"

#: ../src/icon_bar.c:265
msgid "The orientation of the iconbar"
msgstr "アイコンバーが延びる方向です"

#: ../src/icon_bar.c:278
msgid "Icon Bar Model"
msgstr "アイコンバーモデル"

#: ../src/icon_bar.c:279
msgid "Model for the icon bar"
msgstr "アイコンバーのモデルです"

#: ../src/icon_bar.c:295
msgid "Active"
msgstr "アクティブ"

#: ../src/icon_bar.c:296
msgid "Active item index"
msgstr "アクティブなアイテムのインデックスです"

#: ../src/icon_bar.c:312 ../src/icon_bar.c:313
msgid "Show Text"
msgstr "テキストを表示"

#: ../src/icon_bar.c:325
msgid "Scrolled window"
msgstr "スクロールウィンドウ"

#: ../src/icon_bar.c:326
msgid "Scrolled window icon bar is placed into"
msgstr "スクロールされたウィンドウのアイコンバーが配置されます"

#: ../src/icon_bar.c:332 ../src/icon_bar.c:333
msgid "Active item fill color"
msgstr "アクティブなアイテムの塗り色"

#: ../src/icon_bar.c:339 ../src/icon_bar.c:340
msgid "Active item border color"
msgstr "アクティブなアイテムの枠の色"

#: ../src/icon_bar.c:346 ../src/icon_bar.c:347
msgid "Active item text color"
msgstr "アクティブなアイテムのテキストの色"

#: ../src/icon_bar.c:353 ../src/icon_bar.c:354
msgid "Cursor item fill color"
msgstr "カーソルアイテムの塗り色"

#: ../src/icon_bar.c:360 ../src/icon_bar.c:361
msgid "Cursor item border color"
msgstr "カーソルアイテムの枠の色"

#: ../src/icon_bar.c:367 ../src/icon_bar.c:368
msgid "Cursor item text color"
msgstr "カーソルアイテムのテキストの色"

#: ../src/privacy_dialog.c:119
msgid "Time range to clear:"
msgstr "クリア基準:"

#: ../src/privacy_dialog.c:122
msgid "Cleanup"
msgstr "クリーンアップ"

#: ../src/privacy_dialog.c:125
msgid "Last Hour"
msgstr "1 時間以上過去"

#: ../src/privacy_dialog.c:126
msgid "Last Two Hours"
msgstr "2 時間以上過去"

#: ../src/privacy_dialog.c:127
msgid "Last Four Hours"
msgstr "4 時間以上過去"

#: ../src/privacy_dialog.c:128
msgid "Today"
msgstr "今日より過去"

#: ../src/privacy_dialog.c:129
msgid "Everything"
msgstr "すべて"

#: ../src/privacy_dialog.c:152 ../src/xfce_wallpaper_manager.c:264
#: ../src/gnome_wallpaper_manager.c:193
msgid "_Apply"
msgstr "適用(_A)"

#: ../src/privacy_dialog.c:438
msgid "Clear private data"
msgstr "プライベートデータのクリア"

#: ../src/preferences_dialog.c:249
msgid "Display"
msgstr "表示"

#: ../src/preferences_dialog.c:256
msgid "Background color"
msgstr "背景色"

#: ../src/preferences_dialog.c:260
msgid "Override background color:"
msgstr "背景色を変更する:"

#: ../src/preferences_dialog.c:288
msgid "Quality"
msgstr "画質"

#: ../src/preferences_dialog.c:292
msgid ""
"With this option enabled, the maximum image-quality will be limited to the "
"screen-size."
msgstr "最高画質を画面サイズまでに制限することができます。"

#: ../src/preferences_dialog.c:296
msgid "Limit rendering quality"
msgstr "描画品質を画面サイズまでに制限する"

#: ../src/preferences_dialog.c:308
msgid "Fullscreen"
msgstr "全画面"

#: ../src/preferences_dialog.c:312
msgid "Thumbnails"
msgstr "サムネイル"

#: ../src/preferences_dialog.c:315
msgid ""
"The thumbnail bar can be automatically hidden when the window is fullscreen."
msgstr "ウィンドウが全画面化されたときにサムネイルバーを自動的に隠すことができます。"

#: ../src/preferences_dialog.c:319
msgid "Hide thumbnail bar when fullscreen"
msgstr "全画面時にサムネイルを表示しない"

#: ../src/preferences_dialog.c:327
msgid "Clock"
msgstr "時計"

#: ../src/preferences_dialog.c:330
msgid ""
"Show an analog clock that displays the current time when the window is "
"fullscreen"
msgstr "ウィンドウが全画面化されたときに現在の時刻を示すアナログ時計を表示させることができます。"

#: ../src/preferences_dialog.c:334
msgid "Show Fullscreen Clock"
msgstr "全画面時に時計を表示する"

#: ../src/preferences_dialog.c:340
msgid "Mouse cursor"
msgstr "マウスカーソル"

#: ../src/preferences_dialog.c:343
msgid ""
"The mouse cursor can be automatically hidden after a certain period of inactivity\n"
"when the window is fullscreen."
msgstr "ウィンドウが全画面表示の場合、マウスカーソルは一定時間非アクティブな状態が続くと、\n自動的に非表示になります。"

#: ../src/preferences_dialog.c:350
msgid "Period of inactivity (seconds):"
msgstr "非アクティブ期間 (秒):"

#: ../src/preferences_dialog.c:369
msgid "Slideshow"
msgstr "スライドショー"

#: ../src/preferences_dialog.c:373
msgid "Timeout"
msgstr "タイムアウト"

#: ../src/preferences_dialog.c:376
msgid ""
"The time period an individual image is displayed during a slideshow\n"
"(in seconds)"
msgstr "スライドショー中に各画像を表示する時間 (秒)"

#: ../src/preferences_dialog.c:390
msgid "Control"
msgstr "制御"

#: ../src/preferences_dialog.c:394
msgid "Scroll wheel"
msgstr "スクロールホイール"

#: ../src/preferences_dialog.c:397
msgid "Invert zoom direction"
msgstr "ズームの方向を反転する"

#: ../src/preferences_dialog.c:408
msgid "Behaviour"
msgstr "振る舞い"

#: ../src/preferences_dialog.c:412
msgid "Startup"
msgstr "起動"

#: ../src/preferences_dialog.c:414
msgid "Maximize window on startup when opening an image"
msgstr "画像を開いて起動するときにウィンドウを最大化する"

#: ../src/preferences_dialog.c:420
msgid "Wrap around images"
msgstr "最後の画像に来たら先頭に戻る"

#: ../src/preferences_dialog.c:431
msgid "Desktop"
msgstr "デスクトップ"

#: ../src/preferences_dialog.c:538
msgid "Image Viewer Preferences"
msgstr "画像ビューアー設定"

#: ../src/properties_dialog.c:137
msgid "<b>Name:</b>"
msgstr "<b>名前:</b>"

#: ../src/properties_dialog.c:138
msgid "<b>Kind:</b>"
msgstr "<b>種類:</b>"

#: ../src/properties_dialog.c:139
msgid "<b>Modified:</b>"
msgstr "<b>変更日時:</b>"

#: ../src/properties_dialog.c:140
msgid "<b>Accessed:</b>"
msgstr "<b>アクセス日時:</b>"

#: ../src/properties_dialog.c:141
msgid "<b>Size:</b>"
msgstr "<b>サイズ:</b>"

#: ../src/properties_dialog.c:165
msgid "General"
msgstr "一般"

#: ../src/properties_dialog.c:171
msgid "Image"
msgstr "画像"

#: ../src/properties_dialog.c:363
#, c-format
msgid "<b>Date taken:</b>"
msgstr "<b>変更日時:</b>"

#: ../src/properties_dialog.c:375 ../src/properties_dialog.c:387
#: ../src/properties_dialog.c:399
#, c-format
msgid "<b>%s</b>"
msgstr "<b>%s</b>"

#: ../src/properties_dialog.c:458
#, c-format
msgid "%s - Properties"
msgstr "%s - プロパティ"

#: ../src/image_list.c:32
msgid "No supported image type found in the directory"
msgstr "サポートされている種類の画像がディレクトリに見つかりません"

#: ../src/image_list.c:33
msgid "Directory only partially loaded"
msgstr "ディレクトリは部分的にしかロードされていません"

#: ../src/image_list.c:34
msgid "Could not load directory"
msgstr "ディレクトリを読み込めませんでした"

#: ../src/image_list.c:35
msgid "Unsupported mime type"
msgstr "サポートされていない MIME タイプ"

#: ../src/thumbnailer.c:292
msgid ""
"The thumbnailer-service can not be reached,\n"
"for this reason, the thumbnails can not be\n"
"created.\n"
"\n"
"Install <b>Tumbler</b> or another <i>thumbnailing daemon</i>\n"
"to resolve this issue."
msgstr "サムネイラーサービスが利用できないため、\nサムネイルは作成されませんでした。\n\nサムネイルを作成するためには <b>Tumbler</b> もしくはその他の\n<i>サムネイル生成アプリケーション</i>をインストールする必要が\nあります。"

#: ../src/thumbnailer.c:301
msgid "Do _not show this message again"
msgstr "次回からこのメッセージを表示しない(_N)"

#: ../src/xfce_wallpaper_manager.c:241 ../src/gnome_wallpaper_manager.c:176
msgid "Style:"
msgstr "スタイル:"

#: ../src/xfce_wallpaper_manager.c:252
msgid "Apply to all workspaces"
msgstr "すべてのワークスペースに適用"

#: ../src/xfce_wallpaper_manager.c:259 ../src/gnome_wallpaper_manager.c:188
msgid "Set as wallpaper"
msgstr "壁紙に設定"

#: ../src/xfce_wallpaper_manager.c:321 ../src/gnome_wallpaper_manager.c:246
msgid "Auto"
msgstr "自動"

#: ../src/xfce_wallpaper_manager.c:324
msgid "Centered"
msgstr "中央揃え"

#: ../src/xfce_wallpaper_manager.c:327
msgid "Tiled"
msgstr "タイル状"

#: ../src/xfce_wallpaper_manager.c:330
msgid "Stretched"
msgstr "縦横比を維持せず全画面化"

#: ../src/xfce_wallpaper_manager.c:333
msgid "Scaled"
msgstr "長辺を画面に合わせる"

#: ../src/xfce_wallpaper_manager.c:336
msgid "Zoomed"
msgstr "短辺を画面に合わせる"

#: ../org.xfce.ristretto.desktop.in.h:1
msgid "Ristretto Image Viewer"
msgstr "Ristretto 画像ビューアー"

#: ../org.xfce.ristretto.desktop.in.h:2
msgid "Look at your images easily"
msgstr "気軽に画像を見ましょう"

#: ../org.xfce.ristretto.appdata.xml.in.h:1
msgid "Ristretto"
msgstr "Ristretto"

#: ../org.xfce.ristretto.appdata.xml.in.h:2
msgid "Fast and lightweight image viewer"
msgstr "高速で軽量な画像ビューア"

#: ../org.xfce.ristretto.appdata.xml.in.h:4
msgid ""
"The Ristretto Image Viewer is an application that can be used to view and "
"scroll through images, run a slideshow of images, open images with other "
"applications like an image-editor or configure an image as the desktop "
"wallpaper."
msgstr "Ristretto は画像ビューアーアプリケーションです。画像の表示やスクロール、スライドショー、グラフィックエディターなど他のアプリケーションで画像を開く、あるいはデスクトップの壁紙の設定などが行えます。"
